# argumentloader
A simple framework for the [Monkey programming language](https://github.com/blitz-research/monkey), which allows you to load the application-arguments provided by the operating system (Or similar source).
